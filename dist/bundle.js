/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/canvas-events/change-exterior-color.js":
/*!****************************************************!*\
  !*** ./app/canvas-events/change-exterior-color.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return changeExteriorColor; });\nfunction changeExteriorColor() {\n  const exteriorColorSwatch = document.querySelectorAll('.js-changeExteriorEvent');\n  if (!exteriorColorSwatch.length) return;\n  exteriorColorSwatch.forEach(elem => {\n    // getting th evalue of the name attribute from the element\n    const swatchExteriorColor = elem.attributes.name.value; // event to be dispatched once click on the exterior color\n\n    const changeExteriorColorEvent = new CustomEvent('change_exterior_color', {\n      detail: {\n        exterior_color: 'NBA'\n      }\n    });\n    elem.addEventListener('click', () => {\n      console.log('nasser');\n      changeExteriorColorEvent.detail.exterior_color = swatchExteriorColor;\n      window.dispatchEvent(changeExteriorColorEvent);\n    });\n  }); // the response when the the exterior color changes\n\n  window.addEventListener('change_exterior_color_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('change_exterior_color_response: For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/change-exterior-color.js?");

/***/ }),

/***/ "./app/canvas-events/change-interior-color.js":
/*!****************************************************!*\
  !*** ./app/canvas-events/change-interior-color.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return changeInteriorColor; });\nfunction changeInteriorColor() {\n  const interiorColorSwatch = document.querySelectorAll('.js-changeInteriorEvent');\n  if (!interiorColorSwatch.length) return;\n  interiorColorSwatch.forEach(elem => {\n    // getting th evalue of the name attribute from the element\n    const swatchInteriorColor = elem.attributes.name.value; // event to be dispatched once click on the interior color\n\n    const changeInteriorColorEvent = new CustomEvent('change_interior_color', {\n      detail: {\n        interior_color: 'Q'\n      }\n    });\n    elem.addEventListener('click', () => {\n      changeInteriorColorEvent.detail.interior_color = swatchInteriorColor;\n      window.dispatchEvent(changeInteriorColorEvent);\n    });\n  }); // the response when the the Interior color changes\n\n  window.addEventListener('change_interior_color_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('change_interior_color_response: For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/change-interior-color.js?");

/***/ }),

/***/ "./app/canvas-events/enter-car.js":
/*!****************************************!*\
  !*** ./app/canvas-events/enter-car.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return initEnterCarFunctionality; });\nfunction initEnterCarFunctionality() {\n  // defined the Toggle to triger the enter car event\n  const enterCarToggle = document.querySelectorAll('label[for=enter_car]'); // set the initial state of the Incar\n\n  enterCarToggle.forEach(enterCarToggleBtn => {\n    let inCar = false; // event to be dispatched once click on the toggle button\n\n    const enterCarEvent = new CustomEvent('enter_car', {\n      detail: {\n        action: 'enter'\n      }\n    }); // event to be triggered everytime when enter/ exit the car\n\n    window.addEventListener('enter_car_canvas', () => {\n      inCar = !inCar; // eslint-disable-next-line\n\n      enterCarToggleBtn.previousElementSibling.checked = inCar;\n    });\n    enterCarToggleBtn.addEventListener('click', () => {\n      // console.log('button toggled');\n      enterCarEvent.detail.action = inCar ? 'exit' : 'enter';\n      window.dispatchEvent(enterCarEvent);\n    });\n  });\n  window.addEventListener('enter_car_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('enter_car_response: For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/enter-car.js?");

/***/ }),

/***/ "./app/canvas-events/open-doors.js":
/*!*****************************************!*\
  !*** ./app/canvas-events/open-doors.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return initOpenDoorsFunctionality; });\nfunction initOpenDoorsFunctionality() {\n  // defined the Toggle to triger the Open Doors event\n  const openDoorsToggle = document.querySelector('label[for=open_doors]'); // set the initial state of the isDoorsOpen\n\n  let isDoorsOpen = false; // event to be dispatched once click on the toggle button\n\n  const openDoorsEvent = new CustomEvent('open_doors', {\n    detail: {\n      action: 'open'\n    }\n  }); // will be triggered everytime the doors are open or close\n\n  window.addEventListener('open_doors_canvas', () => {\n    setTimeout(() => {\n      isDoorsOpen = !isDoorsOpen;\n      openDoorsToggle.previousElementSibling.checked = isDoorsOpen;\n    }, 0);\n  });\n  window.addEventListener('open_doors_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('open_doors_response: For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n  openDoorsToggle.addEventListener('click', () => {\n    openDoorsEvent.detail.action = isDoorsOpen ? 'close' : 'open';\n    window.dispatchEvent(openDoorsEvent);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/open-doors.js?");

/***/ }),

/***/ "./app/canvas-events/start-engine.js":
/*!*******************************************!*\
  !*** ./app/canvas-events/start-engine.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return initStartEngineFunctionality; });\nfunction initStartEngineFunctionality() {\n  // defined the Toggle to triger the Hotspots event\n  const startEngineToggle = document.querySelector('label[for=start_engine]'); // set the initial state of the isEngineOn\n\n  let isEngineOn = false; // event to be dispatched once click on the toggle button\n\n  const startEngineToggleEvent = new CustomEvent('switch_engine', {\n    detail: {\n      action: 'on'\n    }\n  });\n  window.addEventListener('switch_engine_canvas', () => {\n    setTimeout(() => {\n      isEngineOn = !isEngineOn;\n      startEngineToggle.previousElementSibling.checked = isEngineOn;\n    }, 0);\n  });\n  window.addEventListener('switch_engine_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n  startEngineToggle.addEventListener('click', () => {\n    startEngineToggleEvent.detail.action = isEngineOn ? 'off' : 'on';\n    window.dispatchEvent(startEngineToggleEvent);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/start-engine.js?");

/***/ }),

/***/ "./app/canvas-events/switch-background.js":
/*!************************************************!*\
  !*** ./app/canvas-events/switch-background.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return switchBackground; });\nfunction switchBackground() {\n  // defined the element to change the background\n  const switchBgToggle = document.querySelector('.js-changeBackgroundEvent'); // set the initial state of the backgroundID\n\n  const backgroundID = 1; // event to be dispatched once click on the toggle button\n\n  const switchBgEvent = new CustomEvent('change_background', {\n    detail: {\n      background: 1\n    }\n  });\n  switchBgToggle.addEventListener('click', () => {\n    switchBgEvent.detail.action = backgroundID == 1 ? 2 : 1;\n    window.dispatchEvent(switchBgEvent);\n  });\n  window.addEventListener('change_background_response', event => {\n    // eslint-disable-next-line no-console\n    console.log('change background event: For the BE: check the status in event.detail for any ajax based use case', event.detail);\n  });\n}\n\n//# sourceURL=webpack:///./app/canvas-events/switch-background.js?");

/***/ }),

/***/ "./app/main.js":
/*!*********************!*\
  !*** ./app/main.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../style.scss */ \"./style.scss\");\n/* harmony import */ var _style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_scss__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _canvas_events_enter_car__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./canvas-events/enter-car */ \"./app/canvas-events/enter-car.js\");\n/* harmony import */ var _canvas_events_open_doors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./canvas-events/open-doors */ \"./app/canvas-events/open-doors.js\");\n/* harmony import */ var _canvas_events_start_engine__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./canvas-events/start-engine */ \"./app/canvas-events/start-engine.js\");\n/* harmony import */ var _canvas_events_change_exterior_color__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./canvas-events/change-exterior-color */ \"./app/canvas-events/change-exterior-color.js\");\n/* harmony import */ var _canvas_events_change_interior_color__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./canvas-events/change-interior-color */ \"./app/canvas-events/change-interior-color.js\");\n/* harmony import */ var _canvas_events_switch_background__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./canvas-events/switch-background */ \"./app/canvas-events/switch-background.js\");\n\n // import initSwitchHotspotsFunctionality from './canvas-events/switch-hotspots';\n\n\n // import initShowHotspotsDetailsFunctionality from './canvas-events/show-hotspot-details';\n// import changeGrade from './canvas-events/change-grade';\n\n\n\n\n\n(function initCarConfig() {\n  // initEnterCarFunctionality();\n  // initSwitchHotspotsFunctionality();\n  // initOpenDoorsFunctionality();\n  // initStartEngineFunctionality();\n  // initShowHotspotsDetailsFunctionality();\n  // changeGrade();\n  Object(_canvas_events_change_exterior_color__WEBPACK_IMPORTED_MODULE_4__[\"default\"])();\n  Object(_canvas_events_change_interior_color__WEBPACK_IMPORTED_MODULE_5__[\"default\"])(); // switchBackground();\n})();\n\n//# sourceURL=webpack:///./app/main.js?");

/***/ }),

/***/ "./style.scss":
/*!********************!*\
  !*** ./style.scss ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./style.scss?");

/***/ })

/******/ });