/* eslint-disable */
ASSET_PREFIX = "./dist/configurators/q60/";
SCRIPT_PREFIX = "./dist/configurators/q60/";
SCENE_PATH = "./915813.json";
CONTEXT_OPTIONS = {
    'antialias': true,
    'alpha': false,
    'preserveDrawingBuffer': false,
    'preferWebGl2': true
};
SCRIPTS = [ 30555183, 30555184, 30555152, 30555172, 30555174, 30555164, 30555171, 30555177, 30555176, 30555151, 30554862, 30555181, 30555020, 30555206, 30555099, 30555180, 30555186, 30555153, 30555168, 30555178, 30555165, 30555185, 30555182, 30555173, 30555166, 30555149, 30554864, 30555169, 30555179, 30554863, 30555150, 30554932, 30554946, 31118882, 31118883, 31181533, 31181593 ];
CONFIG_FILENAME = "./dist/configurators/q60/config.json";
INPUT_SETTINGS = {
    useKeyboard: true,
    useMouse: true,
    useGamepads: false,
    useTouch: true
};
pc.script.legacy = false;
PRELOAD_MODULES = [
];
