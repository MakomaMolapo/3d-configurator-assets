#version 300 es
    
#define varying in
out highp vec4 pc_fragColor;
#define gl_FragColor pc_fragColor
#define texture2D texture
#define textureCube texture
#define texture2DProj textureProj
#define texture2DLodEXT textureLod
#define texture2DProjLodEXT textureProjLod
#define textureCubeLodEXT textureLod
#define texture2DGradEXT textureGrad
#define texture2DProjGradEXT textureProjGrad
#define textureCubeGradEXT textureGrad
#define GL2
precision highp float;
#ifdef GL2
precision highp sampler2DShadow;
#endif
varying vec3 vPositionW;
varying vec3 vNormalW;
varying vec2 vUv1;
varying vec2 vUV0_1;

uniform vec3 view_position;
uniform vec3 light_globalAmbient;
float square(float x) {
    return x*x;
}
float saturate(float x) {
    return clamp(x, 0.0, 1.0);
}
vec3 saturate(vec3 x) {
    return clamp(x, vec3(0.0), vec3(1.0));
}
vec4 dReflection;
vec3 dAlbedo;
vec3 dNormalW;
vec3 dVertexNormalW;
vec3 dViewDirW;
vec3 dReflDirW;
vec3 dDiffuseLight;
vec3 dSpecularLight;
vec3 dLightDirNormW;
vec3 dSpecularity;
float dGlossiness;
float dAlpha;
float dAtten;
float dAo;
uniform vec3 light0_color;
uniform vec3 light0_direction;

void getNormal() {
    dNormalW = normalize(dVertexNormalW);
}
vec3 gammaCorrectInput(vec3 color) {
    return pow(color, vec3(2.2));
}
float gammaCorrectInput(float color) {
    return pow(color, 2.2);
}
vec4 gammaCorrectInput(vec4 color) {
    return vec4(pow(color.rgb, vec3(2.2)), color.a);
}
vec4 texture2DSRGB(sampler2D tex, vec2 uv) {
    vec4 rgba = texture2D(tex, uv);
    rgba.rgb = gammaCorrectInput(rgba.rgb);
    return rgba;
}
vec4 texture2DSRGB(sampler2D tex, vec2 uv, float bias) {
    vec4 rgba = texture2D(tex, uv, bias);
    rgba.rgb = gammaCorrectInput(rgba.rgb);
    return rgba;
}
vec4 textureCubeSRGB(samplerCube tex, vec3 uvw) {
    vec4 rgba = textureCube(tex, uvw);
    rgba.rgb = gammaCorrectInput(rgba.rgb);
    return rgba;
}
vec3 gammaCorrectOutput(vec3 color) {
#ifdef HDR
    return color;
#else
    color += vec3(0.0000001);
    return pow(color, vec3(0.45));
#endif
}
uniform float exposure;
vec3 toneMap(vec3 color) {
    return color * exposure;
}
vec3 addFog(vec3 color) {
    return color;
}
vec3 fixSeams(vec3 vec, float mipmapIndex) {
    float scale = 1.0 - exp2(mipmapIndex) / 128.0;
    float M = max(max(abs(vec.x), abs(vec.y)), abs(vec.z));
    if (abs(vec.x) != M) vec.x *= scale;
    if (abs(vec.y) != M) vec.y *= scale;
    if (abs(vec.z) != M) vec.z *= scale;
    return vec;
}
vec3 fixSeams(vec3 vec) {
    float scale = 1.0 - 1.0 / 128.0;
    float M = max(max(abs(vec.x), abs(vec.y)), abs(vec.z));
    if (abs(vec.x) != M) vec.x *= scale;
    if (abs(vec.y) != M) vec.y *= scale;
    if (abs(vec.z) != M) vec.z *= scale;
    return vec;
}
vec3 fixSeamsStatic(vec3 vec, float invRecMipSize) {
    float scale = invRecMipSize;
    float M = max(max(abs(vec.x), abs(vec.y)), abs(vec.z));
    if (abs(vec.x) != M) vec.x *= scale;
    if (abs(vec.y) != M) vec.y *= scale;
    if (abs(vec.z) != M) vec.z *= scale;
    return vec;
}
vec3 cubeMapProject(vec3 dir) {
    return dir;
}
vec3 processEnvironment(vec3 color) {
    return color;
}

#undef MAPFLOAT

#undef MAPCOLOR
 #define MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
#ifdef MAPCOLOR
uniform vec3 material_diffuse;
#endif
#ifdef MAPTEXTURE
uniform sampler2D texture_diffuseMap;
#endif
void getAlbedo() {
    dAlbedo = vec3(1.0);
    #ifdef MAPCOLOR
        dAlbedo *= material_diffuse.rgb;
    #endif
    #ifdef MAPTEXTURE
        dAlbedo *= texture2DSRGB(texture_diffuseMap, UV).CH;
    #endif
    #ifdef MAPVERTEX
        dAlbedo *= gammaCorrectInput(saturate(vVertexColor.VC));
    #endif
}

#undef MAPFLOAT

#undef MAPCOLOR
 #define MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
 #define MAPTEXTURE
#ifdef MAPCOLOR
uniform vec3 material_emissive;
#endif
#ifdef MAPFLOAT
uniform float material_emissiveIntensity;
#endif
#ifdef MAPTEXTURE
uniform sampler2D texture_emissiveMap;
#endif
vec3 getEmission() {
    vec3 emission = vec3(1.0);
    #ifdef MAPFLOAT
        emission *= material_emissiveIntensity;
    #endif
    #ifdef MAPCOLOR
        emission *= material_emissive;
    #endif
    #ifdef MAPTEXTURE
        emission *= texture2DSRGB(texture_emissiveMap, vUV0_1).rgb;
    #endif
    #ifdef MAPVERTEX
        emission *= gammaCorrectInput(saturate(vVertexColor.VC));
    #endif
    return emission;
}
float antiAliasGlossiness(float power) {
    return power;
}

#undef MAPFLOAT
 #define MAPFLOAT

#undef MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
void processMetalness(float metalness) {
    const float dielectricF0 = 0.04;
    dSpecularity = mix(vec3(dielectricF0), dAlbedo, metalness);
    dAlbedo *= 1.0 - metalness;
}
#ifdef MAPFLOAT
uniform float material_metalness;
#endif
#ifdef MAPTEXTURE
uniform sampler2D texture_metalnessMap;
#endif
void getSpecularity() {
    float metalness = 1.0;
    #ifdef MAPFLOAT
        metalness *= material_metalness;
    #endif
    #ifdef MAPTEXTURE
        metalness *= texture2D(texture_metalnessMap, UV).CH;
    #endif
    #ifdef MAPVERTEX
        metalness *= saturate(vVertexColor.VC);
    #endif
    processMetalness(metalness);
}

#undef MAPFLOAT
 #define MAPFLOAT

#undef MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
#ifdef MAPFLOAT
uniform float material_shininess;
#endif
#ifdef MAPTEXTURE
uniform sampler2D texture_glossMap;
#endif
void getGlossiness() {
    dGlossiness = 1.0;
    #ifdef MAPFLOAT
        dGlossiness *= material_shininess;
    #endif
    #ifdef MAPTEXTURE
        dGlossiness *= texture2D(texture_glossMap, UV).CH;
    #endif
    #ifdef MAPVERTEX
        dGlossiness *= saturate(vVertexColor.VC);
    #endif
    dGlossiness += 0.0000001;
}
// Schlick's approximation
uniform float material_fresnelFactor; // unused
void getFresnel() {
    float fresnel = 1.0 - max(dot(dNormalW, dViewDirW), 0.0);
    float fresnel2 = fresnel * fresnel;
    fresnel *= fresnel2 * fresnel2;
    fresnel *= dGlossiness * dGlossiness;
    dSpecularity = dSpecularity + (1.0 - dSpecularity) * fresnel;
}

#undef MAPFLOAT

#undef MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
 #define MAPTEXTURE
#ifdef MAPTEXTURE
uniform sampler2D texture_aoMap;
#endif
void applyAO() {
    dAo = 1.0;
    #ifdef MAPTEXTURE
        dAo *= texture2D(texture_aoMap, vUv1).r;
    #endif
    #ifdef MAPVERTEX
        dAo *= saturate(vVertexColor.VC);
    #endif
    dDiffuseLight *= dAo;
}
void occludeSpecular() {
    float specOcc = dAo;
    dSpecularLight *= specOcc;
    dReflection *= specOcc;
}
uniform samplerCube texture_prefilteredCubeMap128;
uniform samplerCube texture_prefilteredCubeMap64;
uniform samplerCube texture_prefilteredCubeMap32;
uniform samplerCube texture_prefilteredCubeMap16;
uniform samplerCube texture_prefilteredCubeMap8;
#ifndef PMREM4
#define PMREM4
uniform samplerCube texture_prefilteredCubeMap4;
#endif
uniform float material_reflectivity;
void addReflection() {
    // Unfortunately, WebGL doesn't allow us using textureCubeLod. Therefore bunch of nasty workarounds is required.
    // We fix mip0 to 128x128, so code is rather static.
    // Mips smaller than 4x4 aren't great even for diffuse. Don't forget that we don't have bilinear filtering between different faces.
    float bias = saturate(1.0 - dGlossiness) * 5.0; // multiply by max mip level
    int index1 = int(bias);
    int index2 = int(min(bias + 1.0, 7.0));
    vec3 fixedReflDir = fixSeams(cubeMapProject(dReflDirW), bias);
    fixedReflDir.x *= -1.0;
    vec4 cubes[6];
    cubes[0] = textureCube(texture_prefilteredCubeMap128, fixedReflDir);
    cubes[1] = textureCube(texture_prefilteredCubeMap64, fixedReflDir);
    cubes[2] = textureCube(texture_prefilteredCubeMap32, fixedReflDir);
    cubes[3] = textureCube(texture_prefilteredCubeMap16, fixedReflDir);
    cubes[4] = textureCube(texture_prefilteredCubeMap8, fixedReflDir);
    cubes[5] = textureCube(texture_prefilteredCubeMap4, fixedReflDir);
    // Also we don't have dynamic indexing in PS, so...
    vec4 cube[2];
    for(int i = 0; i < 6; i++) {
        if (i == index1) {
            cube[0] = cubes[i];
        }
        if (i == index2) {
            cube[1] = cubes[i];
        }
    }
    // another variant
    /*if (index1==0){ cube[0]=cubes[0];
    }else if (index1==1){ cube[0]=cubes[1];
    }else if (index1==2){ cube[0]=cubes[2];
    }else if (index1==3){ cube[0]=cubes[3];
    }else if (index1==4){ cube[0]=cubes[4];
    }else if (index1==5){ cube[0]=cubes[5];}
    if (index2==0){ cube[1]=cubes[0];
    }else if (index2==1){ cube[1]=cubes[1];
    }else if (index2==2){ cube[1]=cubes[2];
    }else if (index2==3){ cube[1]=cubes[3];
    }else if (index2==4){ cube[1]=cubes[4];
    }else if (index2==5){ cube[1]=cubes[5];}*/
    vec4 cubeFinal = mix(cube[0], cube[1], fract(bias));
    vec3 refl = processEnvironment(gammaCorrectInput(cubeFinal).rgb);
    dReflection += vec4(refl, material_reflectivity);
}
uniform float material_refraction, material_refractionIndex;
vec3 refract2(vec3 viewVec, vec3 Normal, float IOR) {
    float vn = dot(viewVec, Normal);
    float k = 1.0 - IOR * IOR * (1.0 - vn * vn);
    vec3 refrVec = IOR * viewVec - (IOR * vn + sqrt(k)) * Normal;
    return refrVec;
}
void addRefraction() {
    // use same reflection code with refraction vector
    vec3 tmp = dReflDirW;
    vec4 tmp2 = dReflection;
    dReflection = vec4(0.0);
    dReflDirW = refract2(-dViewDirW, dNormalW, material_refractionIndex);
    addReflection();
    dDiffuseLight = mix(dDiffuseLight, dReflection.rgb * dAlbedo, material_refraction);
    dReflDirW = tmp;
    dReflection = tmp2;
}
float getLightDiffuse() {
    return max(dot(dNormalW, -dLightDirNormW), 0.0);
}
// Energy-conserving (hopefully) Blinn-Phong
float getLightSpecular() {
    vec3 h = normalize( -dLightDirNormW + dViewDirW );
    float nh = max( dot( h, dNormalW ), 0.0 );
    float specPow = exp2(dGlossiness * 11.0); // glossiness is linear, power is not; 0 - 2048
    specPow = antiAliasGlossiness(specPow);
    // Hack: On Mac OS X, calling pow with zero for the exponent generates hideous artifacts so bias up a little
    specPow = max(specPow, 0.0001);
    return pow(nh, specPow) * (specPow + 2.0) / 8.0;
}
vec3 combineColor() {
    return mix(dAlbedo * dDiffuseLight, dSpecularLight + dReflection.rgb * dReflection.a, dSpecularity);
}

#undef MAPFLOAT

#undef MAPCOLOR

#undef MAPVERTEX

#undef MAPTEXTURE
 #define MAPTEXTURE
#ifdef MAPTEXTURE
uniform sampler2D texture_lightMap;
#endif
void addLightMap() {
    vec3 lm = vec3(1.0);
    #ifdef MAPTEXTURE
        lm *= texture2DSRGB(texture_lightMap, vUv1).rgb;
    #endif
    #ifdef MAPVERTEX
        lm *= saturate(vVertexColor.VC);
    #endif
    
    dDiffuseLight += lm;
}
void getViewDir() {
    dViewDirW = normalize(view_position - vPositionW);
}
void getReflDir() {
    dReflDirW = normalize(-reflect(dViewDirW, dNormalW));
}

void main(void) {
    dDiffuseLight = vec3(0);
    dSpecularLight = vec3(0);
    dReflection = vec4(0);
    dSpecularity = vec3(0);
   dVertexNormalW = vNormalW;
   dAlpha = 1.0;
   getViewDir();
   getNormal();
   getReflDir();
   getAlbedo();
   getSpecularity();
   getGlossiness();
   getFresnel();
    applyAO();
   addLightMap();
   addReflection();
   dLightDirNormW = light0_direction;
   dAtten = 1.0;
       dAtten *= getLightDiffuse();
       dDiffuseLight += dAtten * light0_color;
       dAtten *= getLightSpecular();
       dSpecularLight += dAtten * light0_color;

   addRefraction();

    occludeSpecular();
   gl_FragColor.rgb = combineColor();
   gl_FragColor.rgb += getEmission();
   gl_FragColor.rgb = addFog(gl_FragColor.rgb);
   #ifndef HDR
    gl_FragColor.rgb = toneMap(gl_FragColor.rgb);
    gl_FragColor.rgb = gammaCorrectOutput(gl_FragColor.rgb);
   #endif
gl_FragColor.a = 1.0;

}