/* eslint-disable */
pc.script.createLoadingScreen(function (app) {
  var polyfill = document.createElement("script");
  polyfill.src = "https://polyfill.io/v3/polyfill.min.js?features=Array.prototype.includes%2CCustomEvent%2CObject.values";

  document.head.appendChild(polyfill);

  var pageBody = document.querySelector("body");
  // adding a loader to the main wrapper to prevent user interaction before car loaded
  pageBody.classList.add("inf-loader");

  var setProgress = function (value) {
    value = Math.min(1, Math.max(0, value));

    var event = new CustomEvent("loading_progress_canvas", {
      detail: {
        status: Math.round(value * 100),
      },
    });

    window.dispatchEvent(event);
  };

  app.on("preload:progress", setProgress);
  app.on("start", function () {
    var event = new CustomEvent("exterior_loaded_canvas", {
      detail: {
        status: true,
      },
    });

    window.dispatchEvent(event);
    // remove the loader once the car is loaded
    pageBody.classList.remove("inf-loader");
  });

  if (window.outerWidth >= 767) {
    app.graphicsDevice.maxPixelRatio = 1;
  } else {
    app.graphicsDevice.maxPixelRatio = window.devicePixelRatio;
  }
});
