import HelperDeviceInfo from '../../../helpers/HelperDeviceInfo';

export default function initSwitchHotspotsFunctionality() {
	// defined the Toggle to triger the Hotspots event
	const switchHotspotsToggle = document.querySelectorAll('label[for=hotspots]');

	let isHotspotsOn = false;
	switchHotspotsToggle.forEach(switchHotspotsToggleBtn => {
		// set the initial state of the isHotspotsOn

		// event to be dispatched once click on the toggle button
		const switchHotspotsEvent = new CustomEvent('switch_hotspot', {
			detail: {
				action: 'on'
			}
		});

		switchHotspotsToggleBtn.addEventListener('click', () => {
			switchHotspotsEvent.detail.action = isHotspotsOn ? 'off' : 'on';

			isHotspotsOn = !isHotspotsOn;
			window.dispatchEvent(switchHotspotsEvent);
			// Added if block to fix hotspot issue in Mobile before this line no 26 is commented outside if block.
			if (new HelperDeviceInfo().isMobile()) {
				// eslint-disable-next-line no-param-reassign
				switchHotspotsToggleBtn.previousElementSibling.checked = isHotspotsOn;
			}
		});
	});

	window.addEventListener('switch_hotspot_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'switch_hotspot_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
