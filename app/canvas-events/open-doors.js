export default function initOpenDoorsFunctionality() {
	// defined the Toggle to triger the Open Doors event
	const openDoorsToggle = document.querySelector('label[for=open_doors]');
	// set the initial state of the isDoorsOpen
	let isDoorsOpen = false;

	// event to be dispatched once click on the toggle button
	const openDoorsEvent = new CustomEvent('open_doors', {
		detail: {
			action: 'open'
		}
	});

	// will be triggered everytime the doors are open or close
	window.addEventListener('open_doors_canvas', () => {
		setTimeout(() => {
			isDoorsOpen = !isDoorsOpen;
			openDoorsToggle.previousElementSibling.checked = isDoorsOpen;
		}, 0);
	});

	window.addEventListener('open_doors_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'open_doors_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});

	openDoorsToggle.addEventListener('click', () => {
		openDoorsEvent.detail.action = isDoorsOpen ? 'close' : 'open';
		window.dispatchEvent(openDoorsEvent);
	});
}
