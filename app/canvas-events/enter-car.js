export default function initEnterCarFunctionality() {
	// defined the Toggle to triger the enter car event
	const enterCarToggle = document.querySelectorAll('label[for=enter_car]');
	// set the initial state of the Incar

	enterCarToggle.forEach(enterCarToggleBtn => {
		let inCar = false;
		// event to be dispatched once click on the toggle button
		const enterCarEvent = new CustomEvent('enter_car', {
			detail: {
				action: 'enter'
			}
		});

		// event to be triggered everytime when enter/ exit the car
		window.addEventListener('enter_car_canvas', () => {
			inCar = !inCar;
			// eslint-disable-next-line
			enterCarToggleBtn.previousElementSibling.checked = inCar;
		});

		enterCarToggleBtn.addEventListener('click', () => {
			// console.log('button toggled');
			enterCarEvent.detail.action = inCar ? 'exit' : 'enter';
			window.dispatchEvent(enterCarEvent);
		});
	});

	window.addEventListener('enter_car_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'enter_car_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
