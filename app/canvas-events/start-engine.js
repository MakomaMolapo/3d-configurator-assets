export default function initStartEngineFunctionality() {
	// defined the Toggle to triger the Hotspots event
	const startEngineToggle = document.querySelector('label[for=start_engine]');
	// set the initial state of the isEngineOn
	let isEngineOn = false;

	// event to be dispatched once click on the toggle button
	const startEngineToggleEvent = new CustomEvent('switch_engine', {
		detail: {
			action: 'on'
		}
	});

	window.addEventListener('switch_engine_canvas', () => {
		setTimeout(() => {
			isEngineOn = !isEngineOn;
			startEngineToggle.previousElementSibling.checked = isEngineOn;
		}, 0);
	});

	window.addEventListener('switch_engine_response', event => {
		// eslint-disable-next-line no-console
		console.log('For the BE: check the status in event.detail for any ajax based use case', event.detail);
	});

	startEngineToggle.addEventListener('click', () => {
		startEngineToggleEvent.detail.action = isEngineOn ? 'off' : 'on';
		window.dispatchEvent(startEngineToggleEvent);
	});
}
