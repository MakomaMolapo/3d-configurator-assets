export default function changeInteriorColor() {
	const interiorColorSwatch = document.querySelectorAll('.js-changeInteriorEvent');

	if (!interiorColorSwatch.length) return;

	interiorColorSwatch.forEach(elem => {
		// getting th evalue of the name attribute from the element
		const swatchInteriorColor = elem.attributes.name.value;

		// event to be dispatched once click on the interior color
		const changeInteriorColorEvent = new CustomEvent('change_interior_color', {
			detail: {
				interior_color: 'Q'
			}
		});

		elem.addEventListener('click', () => {
			changeInteriorColorEvent.detail.interior_color = swatchInteriorColor;
			window.dispatchEvent(changeInteriorColorEvent);
		});
	});

	// the response when the the Interior color changes
	window.addEventListener('change_interior_color_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'change_interior_color_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
