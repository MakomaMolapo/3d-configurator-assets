import toggleWarningMsg from '../helpers/toggleWarningMsg';

export default function changeGrade() {
	const gradeCard = document.querySelectorAll('.js-changeGrade');
	if (!gradeCard.length) return;

	gradeCard.forEach(elem => {
		const gradeName = elem.dataset.grade;
		const gradeExteriorColor = elem.dataset.exterior_color;
		const gradeInteriorColor = elem.dataset.interior_color;

		// event to be dispatched once click on the grade card
		const changeGradeEvent = new CustomEvent('change_grade', {
			detail: {
				grade: 'RS',
				exterior_color: 'NBA',
				interior_color: 'Q'
			}
		});

		elem.addEventListener('click', () => {
			changeGradeEvent.detail.grade = gradeName;
			changeGradeEvent.detail.exterior_color = gradeExteriorColor;
			changeGradeEvent.detail.interior_color = gradeInteriorColor;

			window.dispatchEvent(changeGradeEvent);
		});
	});

	// the response when the grade changes
	window.addEventListener('change_grade_response', event => {
		// in case of failure response
		if (event.detail.status === 'failure') {
			toggleWarningMsg('change_grade_response', true);
		} else {
			toggleWarningMsg('change_grade_response', false);
		}
		// eslint-disable-next-line no-console
		console.log(
			'change_grade_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
