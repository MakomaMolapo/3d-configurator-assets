export default function changeExteriorColor() {
	const exteriorColorSwatch = document.querySelectorAll('.js-changeExteriorEvent');

	if (!exteriorColorSwatch.length) return;

	exteriorColorSwatch.forEach(elem => {
		// getting th evalue of the name attribute from the element
		const swatchExteriorColor = elem.attributes.name.value;

		// event to be dispatched once click on the exterior color
		const changeExteriorColorEvent = new CustomEvent('change_exterior_color', {
			detail: {
				exterior_color: 'NBA'
			}
		});

		elem.addEventListener('click', () => {
			console.log('nasser');
			changeExteriorColorEvent.detail.exterior_color = swatchExteriorColor;
			window.dispatchEvent(changeExteriorColorEvent);
		});
	});

	// the response when the the exterior color changes
	window.addEventListener('change_exterior_color_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'change_exterior_color_response: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
