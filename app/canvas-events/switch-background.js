export default function switchBackground() {
	// defined the element to change the background
	const switchBgToggle = document.querySelector('.js-changeBackgroundEvent');

	// set the initial state of the backgroundID
	const backgroundID = 1;

	// event to be dispatched once click on the toggle button
	const switchBgEvent = new CustomEvent('change_background', {
		detail: {
			background: 1
		}
	});

	switchBgToggle.addEventListener('click', () => {
		switchBgEvent.detail.action = backgroundID == 1 ? 2 : 1;
		window.dispatchEvent(switchBgEvent);
	});

	window.addEventListener('change_background_response', event => {
		// eslint-disable-next-line no-console
		console.log(
			'change background event: For the BE: check the status in event.detail for any ajax based use case',
			event.detail
		);
	});
}
