import toggleSideDrawerVisibility from '../../side-drawer/helpers/toggleSideDrawerVisibility';

export default function initShowHotspotsDetailsFunctionality() {
	const elSideDrawer = document.querySelector('.js__side-drawer');
	window.addEventListener('show_hotspot_details', ({ detail }) => {
		if (!elSideDrawer) return;

		// eslint-disable-next-line no-console
		console.log(
			'show_hotspot_details: For the BE: check the status in event.detail for any ajax based use case',
			detail
		);
		const { classList: drawerClassList } = elSideDrawer;

		// the content of the hotspot panel needs to be populated on each click based on the API response
		toggleSideDrawerVisibility(drawerClassList, 'hotspot-info');
	});
}
