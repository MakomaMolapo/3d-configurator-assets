import '../style.scss';

import changeExteriorColor from './canvas-events/change-exterior-color';
import changeInteriorColor from './canvas-events/change-interior-color';

(function initCarConfig() {
	changeExteriorColor();
	changeInteriorColor();
})();
