# 3D Configurator assets

This repo is only demonstrates how we can interact with 3D canvas, and whats the expected file structure.

## Setup

To setup the project please clone and then simply run:

```sh
npm install
npm start
```

**Then navigate to http://localhost:8080/**

### 3D configurator Files

All the files related to the 3D configurator, including assets, js files, and config file, located under

```sh
/dist/configurators/q60
```


# 3D canvas events
## Event name format:

|Format|Type|Initiator|Example|
|:--|:--|:--|:--|
|`{event_name}`|Request|UI|change_grade|
|`{event_name}_response`|Response|Canvas|change_grade_response|
|`{event_name}_canvas`|Request|Canvas|enter_car_canvas|

---

### change_exterior_color

##### Request

Event initiator - UI

```js
Event name - "change_exterior_color"

Data = {
  detail: {
    exterior_color: "NBA"
  }
}
```

##### Response

Event initiator - canvas

```js
Event name - "change_exterior_color_response"

// success
Data = {
  detail: {
    status: "Success"
  }
}

// error
Data = {
  detail: {
    status: "failure",
    error: [
        { unavailable_color: "Unavailable color. Current grade: RS" }
    ]
  }
}
```
---



### Events handlers Example

an example of how we handle the events on 3d canvas, please check **change-exterior-color.js** file.
**/app/canvas-events/change-exterior-color.js**

### Event example
#### Outgoing from UI to Canvas - you send us when you require change in Canvas (we listen)

`event_name` - change_year, change_grade etc.

`detail` - event data. Cannot be renamed, browser specs. Data inside `detail` can be unique for each event


```js
var event = new CustomEvent('event_name', {
  detail: {
    exterior_color: 'RDS',
  },
});

app.dispatchEvent(event);
```

### Response example
#### Outgoing from Canvas to UI - we send you on event success as response (you listen)

```js
var event = new CustomEvent('event_name', {
  detail: {
    status: 'success',
  },
});

app.dispatchEvent(event);
```

### Action example (Hotspot details event)
#### Outgoing from Canvas to UI. We send you when action in Canvas takes place (you listen)

When user click on feature hotspot configurator trigger next event:

```js
// show_hotspot_details - event_name
var event = new CustomEvent('show_hotspot_details', {
  detail: {
    featureID: 2, // Magento featureID
  },
});

app.dispatchEvent(event);
```
